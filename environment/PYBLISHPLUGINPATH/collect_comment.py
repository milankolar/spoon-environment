import pyblish.api


class CollectComment(pyblish.api.Collector):
    """Adds comment section to GUI
    """

    order = pyblish.api.Collector.order
    label = 'Collect Comment'

    def process(self, context):

        context.data["comment"] = ""
