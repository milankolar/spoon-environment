import os
import pyblish
import pyblish.api
import ft_utils
import pyblish_actions


class RepairPublishPath(pyblish.api.Action):
    label = "Repair"
    on = "failed"
    icon = "wrench"

    def process(self, context):

        path = os.path.dirname(context['publishFile'])

        if not os.path.exists(path):
            os.makedirs(path)


@pyblish.api.log
class ValidatePublishPath(pyblish.api.InstancePlugin):
    """Validates that the publish directory for the workFile exists"""

    order = pyblish.api.ValidatorOrder - 0.1
    label = 'Validate Publish Path'
    families = ['scene']

    actions = [
        RepairPublishPath,
        pyblish_actions.OpenOutputFolder,
        pyblish_actions.OpenOutputFile
        ]

    def process(self, instance):

        workfile = instance.context.data['currentFile']
        # version = instance.context.data['version']
        # version = 'v' + str(version).zfill(3)
        taskid = instance.context.data('ftrackData')['Task']['id']

        ftrack_data = instance.context.data['ftrackData']

        # templates = None
        #
        # if 'Asset_Build' in ftrack_data:
        #     templates = [
        #         'asset.publish.scene'
        #     ]
        # elif 'Shot' in ftrack_data:
        #     templates = [
        #         'shot.publish.scene'
        #     ]
        # else:
        #     templates = [
        #         'folder.publish.scene'
        #     ]

        # assert templates, "Could not recognize entity"

        # self.log.debug(templates)

        # root = instance.context.data('ftrackData')['Project']['root']
        # publishFiles = ft_utils.getPathsYaml(taskid,
        #                                          templateList=templates,
        #                                          version=version,
        #                                          root=root)

        publishFile = workfile

        instance.context.data['publishFile'] = publishFile
        instance.context.data['deadlineInput'] = publishFile
        instance.data['outputPath_publish'] = publishFile
        self.log.debug('Setting publishFile: {}'.format(publishFile))
