import os
import getpass
import launch_tools
from conda_git_deployment import utils

root_dir = os.path.dirname(__file__)

environments = [
    'global',
    'ftrack_connect',
    'pyblish'
    ]

os.environ['PYTHONDONTWRITEBYTECODE'] = '1'
os.environ['STUDIO_USER'] = getpass.getuser()

environment = launch_tools.pass_env(environments)
utils.write_environment(environment)
